﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Yanika_Mifsud_a100268.Startup))]
namespace Yanika_Mifsud_a100268
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
