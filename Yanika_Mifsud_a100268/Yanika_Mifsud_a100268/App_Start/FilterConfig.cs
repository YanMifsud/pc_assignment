﻿using System.Web;
using System.Web.Mvc;

namespace Yanika_Mifsud_a100268
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
