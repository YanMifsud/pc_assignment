﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace Yanika_Mifsud_a100268.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var claims = ClaimsPrincipal.Current.Identities.First().Claims.ToList();
            if (claims.Count <= 1) { ViewBag.loggedin = 0; }
            else { ViewBag.loggedin = 1; }
            
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}