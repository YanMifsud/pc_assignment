﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Yanika_Mifsud_a100268.DataAccess;

namespace Yanika_Mifsud_a100268.Controllers
{
    public class CronJobController : Controller
    {
        public void ExecutePubSub()
        {
            PubSubRepository psr = new PubSubRepository();
            psr.DownloadEmailFromQueueandSend();
        }
    }
}