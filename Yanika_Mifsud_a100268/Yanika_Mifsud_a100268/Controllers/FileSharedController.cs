﻿using Google.Apis.Storage.v1.Data;
using Google.Cloud.Storage.V1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Yanika_Mifsud_a100268.DataAccess;
using Yanika_Mifsud_a100268.Models;

namespace Yanika_Mifsud_a100268.Controllers
{
    public class FileSharedController : Controller
    {
        // GET: FileShared
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Upload(string users, HttpPostedFileBase file)
        {
            //First get user claims    
            var claims = ClaimsPrincipal.Current.Identities.First().Claims.ToList();
            LogRepository lr = new LogRepository();

            try
            {
                if (file != null)
                {
                    var storage = StorageClient.Create();
                    string link = "";

                    using (var f = file.InputStream)
                    {
                        var filename = Guid.NewGuid() + System.IO.Path.GetExtension(file.FileName);
                        var storageObject = storage.UploadObject("wetransferimitation_bucket001", filename, null, f);

                        link = "https://storage.cloud.google.com/wetransferimitation_bucket001/" + filename;

                        if (null == storageObject.Acl)
                        {
                            storageObject.Acl = new List<ObjectAccessControl>();
                        }

                        //Giving Permissions
                        storageObject.Acl.Add(new ObjectAccessControl()
                        {
                            Bucket = "wetransferimitation_bucket001",
                            Entity = $"user-" + claims[0].Value,
                            Role = "OWNER",
                        });

                        string[] individuals = users.Split(',');

                        foreach (string email in individuals)
                        {
                            storageObject.Acl.Add(new ObjectAccessControl()
                            {
                                Bucket = "wetransferimitation_bucket001",
                                Entity = $"user-" + email,
                                Role = "READER", //READER
                            });
                        }

                        var updatedObject = storage.UpdateObject(storageObject, new UpdateObjectOptions()
                        {
                            //Avoid race conditions.
                            IfMetagenerationMatch = storageObject.Metageneration,
                        });
                    }

                    SharedFileRepository sfr = new SharedFileRepository();
                    sfr.AddSharedFile(link, file.ContentType, DateTime.Now, users, Convert.ToInt32(claims[1].Value));

                    //Updating Cache
                    CacheRepository cr = new CacheRepository();
                    cr.UpdateCache(sfr.GetSharedFiles(Convert.ToInt32(claims[1].Value)));

                    lr.WriteLogEntry("File uploaded by " + claims[0].Value + "shared with" + users);

                    PubSubRepository psr = new PubSubRepository();
                    //psr.AddToEmailQueue("Hi, find the following link for the shared file : " + "https://storage.cloud.google.com/wetransferimitation_bucket001/91f96173-3621-45b6-97ac-3995c8dfeb79.docx" + "@" +  "mifsudyan@gmail.com,sciberrasroderick@gmail.com");
                    psr.AddToEmailQueue("Hi, find the following link for the shared file : " + link + "@" + users);
                }

            }
            catch (Exception ex)
            {
                ViewBag.Error = "Product failed to be created; " + ex.Message;
                lr.LogError(ex);
            }

            return View();
        }

        public ActionResult GetSharedFiles() 
        {
            CacheRepository cr = new CacheRepository();
            var sharedfiles = cr.GetSharedFilesFromCache();

            return View("GetSharedFiles", sharedfiles);
        }

        public void Trial()
        {
            string link = "https://cloud.google.com/file.png";
            string sharedusers = "mifsudyan@gmail.com,sciberrasroderick@gmail.com";

            PubSubRepository psr = new PubSubRepository();
            psr.AddToEmailQueue("Hi, find the following link for the shared file : " + link + "@" + sharedusers);

            psr.DownloadEmailFromQueueandSend();
        }
    }
}