﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using System.Web;
using Yanika_Mifsud_a100268.Models;

namespace Yanika_Mifsud_a100268.DataAccess
{
    public class CacheRepository
    {
        private IDatabase db;
        public CacheRepository()
        {
            // var connection = ConnectionMultiplexer.Connect("localhost"); //localhost if cache server is installed locally after downloaded from https://github.com/rgl/redis/downloads 
            var connection = ConnectionMultiplexer.Connect("redis-13557.c16.us-east-1-2.ec2.cloud.redislabs.com:13557,password=vAgEaTSkDmWJcUVsFbIJC0yM0tUG0yYn"); 
            db = connection.GetDatabase();
        }

        public void UpdateCache(List<SharedFile> sharedfiles)
        {
            if (db.KeyExists("sharedfiles") == true)
                db.KeyDelete("sharedfiles");

            string jsonString;
            jsonString = JsonSerializer.Serialize(sharedfiles);

            db.StringSet("sharedfiles", jsonString);
        }

        public List<SharedFile> GetSharedFilesFromCache()
        {
            if (db.KeyExists("sharedfiles") == true)
            {
                var claims = ClaimsPrincipal.Current.Identities.First().Claims.ToList();
                var sharedfiles = JsonSerializer.Deserialize<List<SharedFile>>(db.StringGet("sharedfiles"));
                List<SharedFile> files_owned = new List<SharedFile>();
                foreach (SharedFile sf in sharedfiles) 
                {
                    if (Convert.ToInt32(claims[1].Value) == sf.OwnerFk) { files_owned.Add(sf); }
                }
                return files_owned;
            }
            else
            {
                return new List<SharedFile>();
            }
        }
    }
}