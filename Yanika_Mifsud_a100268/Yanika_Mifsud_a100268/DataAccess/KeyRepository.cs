﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Google.Cloud.Kms.V1;
using System.IO;
using Google.Protobuf;
using System.Text;

namespace Yanika_Mifsud_a100268.DataAccess
{
    public class KeyRepository
    {
        //Working normally with strings
        public string Encrypt(string plaintext)
        {
            KeyManagementServiceClient client = KeyManagementServiceClient.Create();
            CryptoKeyName kn = CryptoKeyName.FromUnparsed(new Google.Api.Gax.UnparsedResourceName("projects/wetransferimitation/locations/global/keyRings/WeTransferImitation_key/cryptoKeys/WeTransferImitation_key001"));
            string cipher = client.Encrypt(kn, ByteString.CopyFromUtf8(plaintext)).Ciphertext.ToBase64();
            return cipher;
        }
        public string Decrypt(string cipher)
        {
            KeyManagementServiceClient client = KeyManagementServiceClient.Create();
            CryptoKeyName kn = CryptoKeyName.FromUnparsed(new Google.Api.Gax.UnparsedResourceName("projects/wetransferimitation/locations/global/keyRings/WeTransferImitation_key/cryptoKeys/WeTransferImitation_key001"));
            //string plaintext = client.Decrypt(kn, ByteString.FromBase64(cipher)).Plaintext.ToStringUtf8();
            string plaintext = client.Decrypt(kn, ByteString.CopyFrom(Convert.FromBase64String(cipher))).Plaintext.ToStringUtf8();
            return plaintext;
        }
    }
}