﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Google.Api;
using Google.Cloud.Diagnostics.AspNet;
using Google.Cloud.Logging.Type;
using Google.Cloud.Logging.V2;

namespace Yanika_Mifsud_a100268.DataAccess
{
    public class LogRepository
    {
        public void LogError(Exception e)
        {
            var exceptionLogger = GoogleExceptionLogger.Create("wetransferimitation", "WeTransferImitation", "1");
            exceptionLogger.Log(e);
        }

        public void WriteLogEntry(string message)
        {
            var logId = "WTI_v1_logs";
            var client = LoggingServiceV2Client.Create();
            LogName logName = new LogName("wetransferimitation", logId);
            LogEntry logEntry = new LogEntry
            {
                LogName = logName.ToString(),
                Severity = LogSeverity.Notice,
                TextPayload = $"{message}"
            };
            MonitoredResource resource = new MonitoredResource { Type = "global" };

            client.WriteLogEntries(logName, resource, null, new[] { logEntry });

        }
    }
}