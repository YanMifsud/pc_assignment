﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Yanika_Mifsud_a100268.DataAccess
{
    public class UserRepository : ConnectionClass
    {
        public UserRepository() : base()
        { }

        public int AddUser(string email, string name)
        {
            string sql = "INSERT into public.user (name, email, lastloggedin) values (@name, @email, @lastloggedin)";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@name", name);
            cmd.Parameters.AddWithValue("@lastloggedin", DateTime.Now);

            MyConnection.Open();
            MyTransaction = MyConnection.BeginTransaction();
            try
            {
                cmd.ExecuteNonQuery();
                MyTransaction.Commit();
            }
            catch { MyTransaction.Rollback(); }
            
            MyConnection.Close();

            string sql2 = "SELECT userid FROM public.user ORDER BY userid DESC LIMIT 1";
            NpgsqlCommand cmd2 = new NpgsqlCommand(sql2, MyConnection);
            MyTransaction = MyConnection.BeginTransaction();
            object result = cmd2.ExecuteScalar();
            result = (result == DBNull.Value) ? null : result;
            int ownerid = Convert.ToInt32(result);
            MyConnection.Close();
            return ownerid;
        }

        public int DoesEmailExist(string email)
        {
            string sql = "SELECT userid FROM public.user WHERE email = @email";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@email", email);

            MyConnection.Open();

            //bool result = Convert.ToBoolean(cmd.ExecuteScalar());
            //int result = Convert.ToInt32(cmd.ExecuteScalar());
            object result = cmd.ExecuteScalar();
            result = (result == DBNull.Value) ? null : result;
            int ownerid = Convert.ToInt32(result);

            MyConnection.Close();

            return ownerid;
        }

        public void UpdateLastLoggedIn(string email)
        {
            string sql = "update public.user set lastloggedin = @lastloggedin where email = @email";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@lastloggedin", DateTime.Now); 

            MyConnection.Open();
            MyTransaction = MyConnection.BeginTransaction();
            try
            {
                cmd.ExecuteNonQuery();
                MyTransaction.Commit();
            }
            catch 
            {
                MyTransaction.Rollback();
            }
            MyConnection.Close();
        }

    }
}