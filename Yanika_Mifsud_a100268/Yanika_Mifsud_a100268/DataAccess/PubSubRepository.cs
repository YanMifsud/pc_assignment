﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Google.Cloud.PubSub.V1;
using System.Text.Json;
using Google.Protobuf;
using Yanika_Mifsud_a100268.Models;
using MimeKit;
using System.IO;
using MailKit.Net.Smtp;
using RestSharp;
using RestSharp.Authenticators;

namespace Yanika_Mifsud_a100268.DataAccess
{
    public class PubSubRepository
    {
        TopicName tn;
        SubscriptionName sn;

        public PubSubRepository()
        {
            tn = new TopicName("wetransferimitation", "EmailTopic");
            sn = new SubscriptionName("wetransferimitation", "EmailSubscription");
        }

        public Topic CreateGetTopic() 
        {
            PublisherServiceApiClient client = PublisherServiceApiClient.Create();
            TopicName tn = new TopicName("wetransferimitation", "EmailTopic");

            try
            {
                return client.GetTopic(tn);
            }
            catch
            {
                return client.CreateTopic(tn);
            }
        }

        private Subscription CreateGetSubscription()
        {
            SubscriberServiceApiClient client = SubscriberServiceApiClient.Create();

            try
            {
                return client.GetSubscription(sn);
            }
            catch
            {
                return client.CreateSubscription(sn, tn, null, 30);
            }

        }

        public void AddToEmailQueue(string msg_recpients) 
        {
            PublisherServiceApiClient client = PublisherServiceApiClient.Create();
            var topic = CreateGetTopic();

            //Encrypt message
            KeyRepository kr = new KeyRepository();
            // String returned base 64
            msg_recpients = kr.Encrypt(msg_recpients);

            // PubSub Messages
            List<PubsubMessage> queuedmessages = new List<PubsubMessage>();
            PubsubMessage pubsubmsg = new PubsubMessage();
            pubsubmsg.Data = ByteString.FromBase64(msg_recpients);
            //pubsubmsg.Data = ByteString.CopyFrom(msg_recpients);
            queuedmessages.Add(pubsubmsg);                        
            // Publish to sub all of the msgs
            client.Publish(topic.TopicName, queuedmessages);
        }

        public void DownloadEmailFromQueueandSend() 
        {
            SubscriberServiceApiClient client = SubscriberServiceApiClient.Create();
            var sub = CreateGetSubscription();
            var pullResponse = client.Pull(sub.SubscriptionName, true, 1);
            string plaintext;         
            
            if (pullResponse != null)
            {
                string cipher = pullResponse.ReceivedMessages[0].Message.Data.ToBase64();

                // Decryption deserialized string
                KeyRepository kr = new KeyRepository();
                plaintext = kr.Decrypt(cipher);

                ///////////////////*********///////////////////
                //Hi, find the following link for the shared file : " +  link + " shared with : " +  users
                int index = plaintext.IndexOf("@");
                // Send emails
                string test = plaintext.Substring(index + 1);
                string[] recipients = test.Split(',');

                for (int i = 0; i < recipients.Length; i++)
                {
                    // Compose a message
                    MimeMessage mail = new MimeMessage();
                    mail.From.Add(new MailboxAddress("Admin", "yan@sandboxf165bf212be54b7ab80fc6b5e8199757.mailgun.org"));
                    mail.To.Add(new MailboxAddress("User", recipients[i]));
                    mail.Subject = "File Share";
                    mail.Body = new TextPart("plain")
                    {
                        Text = @"Hi, here is the link to your shared file : " + plaintext.Substring(0, index)
                    };

                    // Send it!
                    using (var emailclient = new SmtpClient())
                    {
                        emailclient.ServerCertificateValidationCallback = (s, c, h, e) => true;

                        emailclient.Connect("smtp.mailgun.org", 587, false);
                        emailclient.AuthenticationMechanisms.Remove("XOAUTH2");
                        emailclient.Authenticate("postmaster@sandboxf165bf212be54b7ab80fc6b5e8199757.mailgun.org", "2411cc453374d4a7a1ff08137bf91ec5-915161b7-03e01c4c");

                        emailclient.Send(mail);
                        emailclient.Disconnect(true);
                    }
                }


                List<string> acksIds = new List<string>();
                acksIds.Add(pullResponse.ReceivedMessages[0].AckId); //after the email is sent successfully you acknolwedge the message so it is confirmed that it was processed

                client.Acknowledge(sub.SubscriptionName, acksIds.AsEnumerable());
            }
        }



    }
}