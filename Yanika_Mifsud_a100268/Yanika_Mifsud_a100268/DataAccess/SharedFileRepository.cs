﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Yanika_Mifsud_a100268.Models;

namespace Yanika_Mifsud_a100268.DataAccess
{
    public class SharedFileRepository : ConnectionClass
    {
        public SharedFileRepository() : base() { }

        public List<SharedFile> GetSharedFiles(int ownerfk)
        {
            string sql = "Select sharedfileID, title, format, uploaded, sharedwith from sharedfile where ownerfk = @ownerfk";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);

            MyConnection.Open();
            List<SharedFile> results = new List<SharedFile>();

            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    SharedFile sf = new SharedFile();
                    sf.sharedfileID = reader.GetInt32(0);
                    sf.Title = reader.GetString(1);
                    sf.Format = reader.GetString(2);
                    sf.Uploaded = reader.GetDateTime(3);
                    sf.SharedWith = reader.GetString(4);
                    sf.OwnerFk = ownerfk;
                    results.Add(sf);
                }
            }

            MyConnection.Close();

            return results;
        }

        public void AddSharedFile(string title, string format, DateTime uploaded, string sharedwith, int ownerfk)
        {
            string sql = "INSERT into public.sharedfile (title, format, uploaded, sharedwith, ownerfk) values (@title, @format, @uploaded, @sharedwith, @ownerfk)";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@title", title);
            cmd.Parameters.AddWithValue("@format", format);
            cmd.Parameters.AddWithValue("@uploaded", DateTime.Now);
            cmd.Parameters.AddWithValue("@sharedwith", sharedwith);
            cmd.Parameters.AddWithValue("@ownerfk", ownerfk);

            MyConnection.Open();
            MyTransaction = MyConnection.BeginTransaction();

            try
            {
                cmd.ExecuteNonQuery();
                MyTransaction.Commit();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                MyTransaction.Rollback();
            }
            
            MyConnection.Close();
        }
    }
}