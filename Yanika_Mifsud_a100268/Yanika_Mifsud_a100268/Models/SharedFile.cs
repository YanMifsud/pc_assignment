﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Yanika_Mifsud_a100268.Models
{
    public class SharedFile
    {
        public int sharedfileID { get; set; }
        public string Title { get; set; }
        public string Format { get; set; }
        public DateTime Uploaded { get; set; }
        public string SharedWith { get; set; } // Comma separated values
        public int OwnerFk { get; set; }
    }
}