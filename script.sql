create table if not exists "user"
(
    userid       serial       not null
        constraint user_pk
            primary key,
    name         varchar(255) not null,
    email        varchar(255) not null,
    lastloggedin timestamp
);

alter table "user"
    owner to postgres;

create unique index if not exists user_userid_uindex
    on "user" (userid);

create table if not exists sharedfile
(
    sharedfileid serial       not null
        constraint sharedfile_pk
            primary key,
    title        varchar(255) not null,
    format       varchar(255) not null,
    uploaded     timestamp    not null,
    ownerfk      integer      not null
        constraint ownerfk
            references "user",
    sharedwith   varchar(255)
);

alter table sharedfile
    owner to postgres;

create unique index if not exists sharedfile_productid_uindex
    on sharedfile (sharedfileid);


